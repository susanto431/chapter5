const express = require('express');
const app = express();

// app.[method](path end point)
app.get('/', (req, res) => {
    res.send("Hello World")
});

// Object => Product

// Butuh API yg bisa request utk membuat / create sebuah produk
app.post('/product', (req, res) => {
    res.send(200, "Data Product Posted")
});
// Butuh API yg bisa request utk melihat semua data produk
app.get('/product', (req, res) => {
    res.send("All Data Product")
});
// Butuh API yg bisa request utk mengubah semua data produk
app.put('/product', (req, res) => {
    res.send("Mengubah Data Product")
});
// Butuh API yg bisa request utk mendelete semua data produk
app.delete('/product', (req, res) => {
    res.send("Mendelete Data Product")
});

// Object => Customer

// Butuh API yg bisa request utk membuat / create sebuah produk
app.post('/customer', (req, res) => {
    res.send("Data customer Posted")
});
// Butuh API yg bisa request utk melihat semua data produk
app.get('/customer', (req, res) => {
    res.send("All Data customer")
});
// Butuh API yg bisa request utk mengubah semua data produk
app.put('/customer', (req, res) => {
    res.send("Mengubah Data customer")
});
// Butuh API yg bisa request utk mendelete semua data produk
app.delete('/customer', (req, res) => {
    res.send("Mendelete Data customer")
});

app.listen(3000, () => {
    console.log(`Server started on 3000`);
});


// CRUD => BASIC FUNCTION
// CREATE
// READ
// UPDATE
// DELETE

// Layanan order => create order, nampilin data order, update data order, delete data order
// Layanan customer => create customer, nampilin data customer, update data customer, delete customer.

// list semua service yang ada:

// Misal Topik : e - commerce
// maka perlu jelaskan kata benda utk topik e-commerce
// product
// customer
// kurir

// kl sdh habis kata benda, ganti => Fungsi utama ??
// order
// login



















// const http = require('http');

// function onRequest(request, response) {
//     response.writeHead(200, { "Content-Type": "application/json" })

//     const data = [
//         {
//             nama: "Susanto",
//             usia: 32,
//             email: "susanto@gmail.com"
//         },
//         {
//             nama: "Kartika",
//             usia: 32,
//             email: "kartika@gmail.com"
//         }]
//     response.end(JSON.stringify(data))
// }
// http.createServer(onRequest).listen(8000)